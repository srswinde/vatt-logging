#!/usr/bin/python3



import matplotlib as mpl
mpl.use('Agg')
from vattlog import ngtelemSQL
from vattlog import  bintelemSQL, bintempsSQL, binrelaySQL, secondary_data, secondary_indimsg
import matplotlib.pyplot as plt
import datetime
from dateutil import parser
import matplotlib.pyplot as plt
import sys


assert sys.version_info > ( 3,0 ) 


"""
    Examples of how to use the data loggers in the vattlog utility.
"""


def makecsv(ts=None, dt=90 ):
    """Make CSV files from the vattlogs of bintelemSQL, binrelaySQL, ngtelemSQL from
        a certain datetime."""

    if ts is None:
        ts = datetime.datetime( 2018, 3, 19, 21, 0, 0, 0 )

    datadict = {}
    for logger in ( bintelemSQL(), binrelaySQL(), bintempsSQL(), ngtelemSQL() ):
        #retrieve data as a pandas dataframe and put it in a csv file. 
        data = logger.after(  ts, dt )
        data.to_csv( "{}.csv".format( logger.__tablename__) )
    


def makeplot( ts=None, dt=90 ):
    """Make a png plot of the bintelemSQL, binrelaySQL, ngtelemSQL,    
        tables at a given time which defautls to March 3rd at midnight.
        for 90 minutes. 
    """
    if ts is None:
        ts = datetime.datetime( 2018, 3, 19, 21, 0, 0, 0 )

    datadict = {}
    for logger in ( bintelemSQL(), bintempsSQL(), binrelaySQL() ):
        logger.plot(ts, dt)

        plt.savefig( "{}.png".format( logger.__tablename__) )


def makecsv_recent( dt=90 ):
        
    """Make csv the bintelemSQL, binrelaySQL, ngtelemSQL,    
       for the most recent data going back dt minutes 
    """

    for logger in ( bintelemSQL(), binrelaySQL(), ngtelemSQL() ):
        #retrieve data as a pandas dataframe and put it in a csv file. 
        data = logger.recent( minsago=dt )
        data.to_csv( "{}.csv".format( logger.__tablename__) )
        
        
        

def makeplot_recent( dt=90 ):
        
    """Make a png plot of the bintelemSQL, binrelaySQL, ngtelemSQL,    
       for the most recent data going back dt minutes 
    """

    for logger in ( bintelemSQL(), binrelaySQL(), ngtelemSQL() ):
        logger.plot_recent(dt)
        plt.savefig( "{}.png".format( logger.__tablename__) )

       
    
        
        
 
