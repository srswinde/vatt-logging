#!/usr/bin/python3

from vattlog import ngtelem, bintelem, secondary
import time


"""
    A very simple script to log all available devices
    in a loop.
"""



def main():


    getters =  [ bintelem(), ngtelem(), secondary() ]
    while 1:
        for getter in getters:

            try:
                dt=getter.getdata()
            except Exception as err:
                print( time.ctime(), getter, err )
            time.sleep( 1.0 )




main()
