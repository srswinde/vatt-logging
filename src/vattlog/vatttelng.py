from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Float, SmallInteger, DATETIME
from telescope import telescope
from astro.angles import Hour_angle, Deg10, RA_angle, Dec_angle
import json
import datetime
from .baseclasses import logger, getter
Base = declarative_base()
from collections import OrderedDict


class ngtelemSQL( Base, logger ): # notice the multi inheritance

    __tablename__ = "ngtelem"
    id = Column(Integer, primary_key=True)
    
    altitude = Column( Float( ( 7,4 ) ) )
    azimuth = Column( Float( ( 7,4 ) ) )
    declination = Column( Float( ( 7,5 ) ) )
    hour_angle = Column( Float( ( 7, 4 ) ) )
    sidereal_time = Column( Float( ( 7, 4 ) ) )
    motion  = Column( SmallInteger  )
    right_ascension = Column( Float( (7, 4) ) )
    airmass = Column( Float( ( 4, 3 ) ) )
    timestamp = Column ( DATETIME )

    def __init__(self):

        """logger constructor has to be called explicitly
        because this class has multiple inheritance"""
        
        logger.__init__(self)

        self.ColumnDescription =  OrderedDict(
            altitude = "Altitude/Elevation angle",
            azimuth = "Azimuth angle",
            declination = "Declination",
            hour_angle = "Hour Angle",
            sidereal_time = "Local Sidereal Time",
            motion = "Motion Bit",
            right_ascension = "Right Ascension",
            airmass = "Airmass (secant Z)",
            timestamp = "Timestamp",
        )
        self.description = "NG ALL Telemetry"


class ngtelem( getter ):


    def __init__(self):
        super(self.__class__, self  ).__init__()
        self.current_data={}
        

    def getdata( self, log=True ):
        """"""
        server = self.cfg["vatttel_ng_addr"][0]
        name = "VATT"
        try:
            vatt = telescope( server, name )
        
            data = vatt.reqALL()
        except Exception as err:
            return {}
        self.current_data["altitude"] = float( data['alt'] )
        self.current_data["azimuth"] = float( data['az'] )
        self.current_data["declination"] = Dec_angle( data['dec'] )
        self.current_data["hour_angle"] = Hour_angle(data['ha'])
        self.current_data["sidereal_time"] = RA_angle(data['lst'])
        self.current_data["motion"] = int(data['motion'])
        self.current_data["right_ascension"] = RA_angle( data['ra'] )
        self.current_data["airmass"] = float( data['secz'] )
        self.current_data["timestamp"] = datetime.datetime.now()

        if log:
            logger = ngtelemSQL()
            logger.load_data( self.current_data )
            logger.log()

        return self.current_data




    

        
