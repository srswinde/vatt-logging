from sqlalchemy.ext.declarative import declarative_base

import xml.etree.ElementTree as ET


from sqlalchemy import Column, Integer, Float, DATETIME, Text, BOOLEAN
import select
import socket
from .baseclasses import logger, getter
import errno
import datetime
import pytz
from collections import OrderedDict

Base = declarative_base()


class secondary_data( Base, logger ):

    """
        Secondary data class ORM.
    """

    id = Column( Integer, primary_key=True )
    __tablename__ = "secondary_data"
    posx = Column( Float )
    posy = Column( Float, nullable=True )
    posz = Column( Float, nullable=True )
    tipx = Column( Float, nullable=True )
    tipy = Column( Float, nullable=True )

    temp = Column( Float, nullable=True )
    correct = Column( BOOLEAN, nullable=True )
    timestamp = Column( DATETIME )

    def __init__( self ):

        logger.__init__( self )
        self.ColumnDescription = OrderedDict(

            posx="X Position",
            posy="Y Position",
            posz="Z Position",
            tipx="Angle from X axis",
            tipy="Angle form Y axis",
            temp="Strut Temperature for correction",
            correct="Autocollimate State",
            timestamp="Timestamp",
        )
        self.description = "Secondary Data"


class secondary_indimsg( Base, logger ):

    id = Column( Integer, primary_key=True )
    __tablename__ = "secondary_indimsg"
    msg = Column( Text( 150 ) )
    timestamp = Column( DATETIME, nullable=True )

    def __init__( self, xmltag=None ):

        if xmltag is not None:
            dt = datetime.datetime.strptime(
                xmltag.attrib["timestamp"],
                "%Y-%m-%dT%H:%M:%S" )

            dt = pytz.utc.localize( dt )
            self.timestamp = dt.astimezone( pytz.timezone("US/Arizona") )
            self.msg = xmltag.attrib["message"]

        logger.__init__( self )
        self.ColumnDescription = OrderedDict(
            msg="Message from secondary INDI driver",
            timestamp="Timestamp",
        )
        self.description = "Secondary INDI Messages"


class secondary( getter ):
    """secondary class
        The secondary needs two tables, one for indi messages
        and the other for indi properties. This class gets
        data from the indi server and seperates it into
        those two tables.

        Hopefully it will alsio provide a single transparent interface
        to the sql orm



    """

    logger = secondary_data

    socket = None

    def __init__(self):
        super( self.__class__, self ).__init__()
        self.current_data = {}
        self.xml = None
        self.imsgs = []

    def open_socket( self ):
        print("opening socket")
        self.socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.socket.connect( tuple(self.cfg[ "secondary_indi_addr" ]) )

        self.socket.settimeout(0.5)
        self.socket.send("<getProperties  version='1.7' />\n".encode())

    def getdata( self, log=True ):
        """
            Grab data from indiserver
            This is done by keeping a socket connection open and non
            -blocking and using select to to wait for data.

        """
        # Opent the socket if its not open
        if self.socket is None:
            self.open_socket()

        inputs = [self.socket]
        try:
            readable, writeable, err = select.select( inputs, [], [], 0.1 )
        except OSError as err:
            if err.errno == errno.EBADF:
                self.socket = None

        chunk = ""
        resp = ""
        for conn in readable:
            reading = True
            chunks = 0
            while reading:
                try:
                    # hard coded chunksize of 1024 byte
                    chunk = conn.recv(1024)
                    resp += chunk.decode("utf-8")

                    chunks += 1

                except socket.timeout:
                    break

                except OSError as err:
                    if err.errno is not errno.EAGAIN:
                        raise
                    else:
                        break

        if resp is not "":
            xmlstring = "<XMLindiData>\n {} \n</XMLindiData>\n".format( resp )
            try:
                xml = ET.fromstring( xmlstring  )

            except ET.ParseError as err:
                print( err )
                xml = None
        else:
            xml = None
        self.xml = xml
        ivpsql = None

        if xml is not None:
            retn = self.parse_xml( xml )

            if log:
                sd = secondary_data()
                sd.load_data( retn )
                sd.log()
                for msg in self.imsgs:
                    msg.log()
            return retn

    def to_sql( self ):
        # it is easier to do this inside the
        # getdata function
        pass

    def parse_xml( self, xml ):

        # The sqlalchemy class to log data.

        for message_or_data in xml:

            if message_or_data.tag == "message":
                self.imsgs.append( secondary_indimsg( message_or_data ) )

            else:

                """Take the xml vector property log it if it corresponds to one
                of the columns in this table"""
                xmltag = message_or_data
                name = xmltag.attrib["name"]

                # time from indi is in utc here we convert
                try:
                    dt = datetime.datetime.strptime(
                        xmltag.attrib["timestamp"], "%Y-%m-%dT%H:%M:%S" )
                except KeyError:
                    print("No timestamp in")

                    for tag in xmltag:
                        print((tag.tag))
                        print((tag.attrib))
                    dt = datetime.datetime.now()
                # let datetime know that it is UTCS
                try:
                    dt = pytz.utc.localize( dt  )
                except ValueError as err:
                    print( "timetsamp already not naive" )

                # then convert to arizona
                self.timestamp = dt.astimezone(pytz.timezone("US/Arizona"))

                if name == "PosX":
                    self.current_data["posx"] = float( xmltag[0].text )

                elif name == "PosY":
                    self.current_data["posy"] = float( xmltag[0].text )

                elif name == "PosZ":
                    self.current_data["posz"] = float( xmltag[0].text )

                elif name == "temp":
                    self.current_data["temp"] = float( xmltag[0].text )

                elif name == "PosV":
                    self.current_data["tipx"] = float( xmltag[0].text )

                elif name == "PosU":
                    self.current_data["tipy"] = float( xmltag[0].text )

                elif name == "correct":
                    txt = xmltag[0].text.strip()
                    val = None
                    if txt == "On":
                        val = True
                    elif txt == "Off":
                        val = False

                    else:
                        raise AttributeError(
                            "Value for correct switch (or any INDI switch \
                            should be 'On' or 'Off' not {})".format(txt)  )
                    self.current_data["correct"] = float( val )

        return self.current_data

    def close(self):
        if self.socket is not None:
            self.socket.close()

    def __del__( self ):
        self.close()




"""

The following classes are for turning indi xml
into convenient classes. I think this will be usefull
for future uses of python with indi but, got in the
way of simple logging interface I was creating above.
I will keep them here for now but they can be ignored.

class INDIVector(object):

    def __new__( cls, *args, **kwargs ):
        xml = args[0]
        if xml.tag == "defSwitchVector":
            obj = object.__new__(INDISwitchVector, cls, xml)
            return obj
        elif xml.tag == "defTextVector":
            obj = object.__new__(INDITextVector, cls, xml)
            return obj
        elif xml.tag == "defNumberVector":
            obj = object.__new__(INDINumberVector, cls, xml)
            return obj

        elif xml.tag == "message" :
            obj = INDImsg(xml)
            return obj

        else:
            return super(INDIVector, cls).__new__(cls, xml)

    def __init__( self, xml ):
        self.device = xml.attrib["device"]
        self.group = xml.attrib["group"]
        self.label = xml.attrib["label"]
        self.name = xml.attrib["name"]
        self.perm = xml.attrib["perm"]
        self.state = xml.attrib["state"]
        self.timeout = xml.attrib["timeout"]

        self.timestamp = xml.attrib["timestamp"]
        self.attrib = xml.attrib

        if self.__class__ == INDITextVector:
            self.indi_properties = self.texts


        elif self.__class__ == INDISwitchVector:
            self.indi_properties = self.switches


        elif self.__class__ == INDINumberVector:
            self.indi_properties = self.numbers





    def __getattr__(self, attr):
        if attr in self.attrib:
            return self.attrib[attr]
        else:
            raise AttributeError


    def __getitem__( self, key ):
        if type(key) == int:
            return self.indi_properties[key]

        elif type(key) == str or type(key) == unicode:
            for prop in self.indi_properties:
                if prop.name == key:
                    return prop

        raise ValueError("INID properties must be indexed by name or number not {}".format( key ))


class INDISwitchVector( INDIVector  ):

    def __init__( self, xml ):

        self.rule = xml.attrib["rule"]
        self.switches = [ INDISwitch(swxml) for swxml in xml ]

        INDIVector.__init__( self, xml )

class INDITextVector( INDIVector ):

    def __init__( self, xml ):

        self.texts = [INDIText(xmltag) for xmltag in xml ]

        INDIVector.__init__( self, xml )

class INDINumberVector( INDIVector ):

    def __init__( self, xml ):

        self.numbers = [INDINumber(xmltag) for xmltag in xml ]

        INDIVector.__init__( self, xml )


class INDIProperty( object ):


    def __init__( self, xml ):
        self.label = xml.attrib['label']
        self.name = xml.attrib["name"]

        self.attrib = xml.attrib

    def __getattr__(self, attr):
        if attr in self.attrib:
            return self.attrib[attr]
        else:
            raise AttributeError("This attribute doesn't exist: {}".format(attr))


class INDISwitch(INDIProperty):

    def __init__(self, xml):
        INDIProperty.__init__(self, xml)
        self.state = xml.text.strip()

    def __bool__(self)   :
            if self.state == "On":
                return True
            elif self.state == "Off":
                return False

            else:
                raise ValueError( "Value of INDISwitch {} not allowed {} ".format(self.name, self.value) )

class INDImsg:

    def __init__(self, xml):
        self.device = xml.attrib["device"]
        self.timestamp = datetime.datetime.strptime( xml.attrib["timestamp"], "%Y-%m-%dT%H:%M:%S" )


class INDIText( INDIProperty ):
    def __init__(self, xml):
        INDIProperty.__init__(self, xml)
        self.text = xml.text


class INDINumber( INDIProperty ):
    def __init__(self, xml):
        INDIProperty.__init__(self, xml)
        self.value = float( xml.text )




    def __float__(self):
        return self.value





"""
