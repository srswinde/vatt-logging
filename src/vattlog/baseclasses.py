from sqlalchemy import create_engine, Column, Integer, String, Float, SmallInteger, DATETIME
from sqlalchemy.orm import sessionmaker
import pandas as pd
import json
import datetime
import os
import json
import sqlite3
import errno
import sys

#raise an error if python2
assert sys.version_info > (3,0)



CFGPATH = "/home/scott/.mtnops/logcfg.json"


class config(object):
    """Config file stuff. """
    def __init__( self ):
        if os.path.exists(CFGPATH):
            self.cfg = json.load( open( CFGPATH ) )

        else:
            #defaults
            self.cfg = {
                "dbpath"                : "test3.db",
                "secondary_indi_addr"   : ["10.0.1.108", 7624 ],
                "vatttel_bin_addr"      : ["10.0.1.10", 1040],
                "vatttel_ng_addr"        : ["10.0.1.10", 5750]
            }





class getter( config ):
    """
        This is the superclass of all classes designed for getting data 
        from each device. Each subclass should be associated with a single
        device and overwrite the getdata method. This method should define
        how to get data from the associated device and how to log the data.
        
        The logging should be done with a subclass of the logger class.

        Initially, I wanted the method that stores the data in the 
        database to be written at this level (to_sql) but the ways in which 
        getter classes recv and store data is varied and complex. So that 
        should be done in the subclass.
        
    """

    def __init__( self ):
        super(getter, self).__init__()
       

    def to_sql( self ):
        raise NotImplementedError( "Logging into sql db should be done in sub class not here" )
                    

    def getdata( self, log=None ):
        raise NotImplementedError( "This method should be overwritten in a sub class" )
            

    def sql_interface( self ):
        return self.logger()


    def recent(self, minsago=30):
        sqli = self.sql_interface()
        return sqli.recent(minsago=minsago)


class logger( config ):
    """Description: 
        Subclasses of this class define the sql tables for logging. 
        This base class simply provides a few convienience methods
        for retrieving data from the DB. 
    """
       


    def __init__( self  ):
        #all subclasses should have access to config file
        super(logger, self).__init__()
        assert hasattr(self, "timestamp"), "Each sql table must have a timestamp column for better indexing. {} does not".format( self.__str__() )
        

        if not os.path.exists( self.cfg[ "dbpath" ] ):
            #create the db if it doesn't exist
            try:
                if os.path.dirname(self.cfg[ "dbpath" ]) != "":
                    os.makedirs( os.path.dirname( self.cfg[ "dbpath" ] ) )
            except OSError as err:
                print(( err, self.cfg[ "dbpath" ] ))
                if err.errno == errno.EEXIST and os.path.isdir( os.path.dirname( self.cfg[ "dbpath" ] ) ):
                    pass
                else:
                    raise
            sqlite3.connect( self.cfg[ "dbpath" ] )
        engine = create_engine( "sqlite:///{}".format( self.cfg[ "dbpath" ] ))
        self.metadata.bind = engine
        self.metadata.create_all() # create the database if it is not there
            

    def load_data( self, data ):
        for key, val in data.items():
            setattr( self, key, val )

        if self.timestamp is None:
            self.timestamp = datetime.datetime.now()

 
               
    def bounded_session( self ):
            engine = create_engine( "sqlite:///{}".format( self.cfg[ "dbpath" ] ) )
            session = sessionmaker( bind=engine )()
            return session


    def log( self, data=None ):
        session = self.bounded_session()
        session.add( self )
        session.commit( )
        session.close( )
        

    def query( self ):
        session = self.bounded_session()
        return session.query( self.__class__  )


    def after( self, aftertime, deltamins=30 ):
        
        session = self.bounded_session()
        dt = datetime.timedelta( minutes=deltamins )
        qr = session.query( self.__class__ ).filter( self.__class__.timestamp > aftertime ).filter( self.__class__.timestamp < (aftertime+dt) )
        ex = session.bind.execute( qr.selectable )
        data = pd.DataFrame( ex.fetchall() )

        
        if data.empty:
            return None

        data.columns = [ key.replace( self.__tablename__+"_", '' ) for key in list(ex.keys()) ]
        out_columns = [key.replace( self.__tablename__+"_", '' ) for key in list(ex.keys()) if key not in ('id', )]
        
        data.set_index( 'timestamp', inplace=True )

        session.close()

        # return the data frame but without the database primary key "id"
        return data[[col for col in data.columns if col not in ('id', )]]
        
    def plot( self, aftertime, deltamins, **kwargs ):
        data=self.after( aftertime, deltamins )
        return data.plot( subplots=True )


    def plot_recent( self, minsago=30, **kwargs ):
        data = self.recent(minsago)
        return data.plot( subplots=True )


    def recent( self, minsago=30 ):
        now = datetime.datetime.now()
        dt = datetime.timedelta( minutes=minsago )
        start = now - dt

        return self.after( start, minsago )


