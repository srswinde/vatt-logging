# Description

## Overall
This is the start of the overall logging of most systems 
at VATT. I hope that it will also be the precursor to the 
state machine as we move into robotization. 

## Where does it run
It is meant to be run on vattocs but could be run on 
any computer behind the vatt firewall that supports 
python3. 


## How does it work
Each device that has information to log has 2 or more classes
associated with it. The first class is called the getter
which polls or gets data from the device. This is done
by overriding the "getdata" method in all subclasses 
of "getter". The other classes associated with the device
are the logging classes. They define the database tables where information is
logged. Currently we are using sqlalchemy and sqlite for logging.


# Install

`sudo python setup.py install`

Don't forget the config file. It is expected in /home/scott/.mtnops
but that can be changed in the baseclasses.py file


# List of available logs


| **Log object** | **Getter Name** | **Description** |
|-------------|-------------|-------------|
| ngtelemSQL  | ngtelem     |ng style data from vatttel|
| bintelemSQL | bintelem		|Binary telemetry data from vatttel (mnelson server)|
| bintempsSQL | bintelem    |binary temperature data from vatttel (mnelson server)|
| binrelaySQL | bintelem    |binary relay data from vatttel (mnelson server)|
| secondary_data| vattsecondary| Position and other data on the secondary|             
| secondary_indimsg| vattsecondary| INDI messages from the vattsecondary indi driver|
|             |             |             |
|             |             |             |



# Examples

**Probably the best examples will be found in scripts/examples.py. Take a look
there first! Also you can look at [the jupyterlab session on google colaboratory](https://colab.research.google.com/drive/1B-GJpkvAL-jv5Xnft74S9NjllDxQqTiD)**


```python
#!/usr/bin/python

"""
	Get the last 45 minutes of logs from the ngtelemSQL log object 
  and write them to csv file.  
"""
from vattlog import ngtelemSQL #the ngtelem logger class

n=ngtelemSQL() #instantiate the class
data = n.recent(45) #get the data from the last 45 minutes into a nice pandas dataframe.
data.to_csv("last45.csv")


```



