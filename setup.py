#!/usr/bin/env python3

from distutils.core import setup
import shutil
from subprocess import call


shutil.copy("systemd/vattlogger.service", "/etc/systemd/system/")
call(["chmod", "664", "/etc/systemd/system/vattlogger.service"])
call(["systemctl", "daemon-reload"])
call( [ "systemctl", "enable", "vattlogger.service"  ] )



setup(name='vattlogger',
      version='1.0',
      description='VATT logger',
      author='Scott Swindell',
      author_email='scottswindell@email.arizona.edu',
          package_dir = {'':'src'}, 
        packages=['vattlog'],
        scripts = ["scripts/dologging.py"],
     )
